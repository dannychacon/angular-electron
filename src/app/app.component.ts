import {Component, HostListener, OnInit} from '@angular/core';
import {MenuItem} from 'primeng/primeng';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app works!';
  private items: MenuItem[];


  constructor(private router: Router) {
  }

  ngOnInit() {
    this.items = [
      {
        label: 'File',
        items: [{
          label: 'New',
          icon: 'fa-plus',
          items: [
            {label: 'Customer', routerLink: '/customer'},
            {label: 'Other'},
          ]
        },
          {label: 'Dashboard', routerLink: '/dashboard'},
          {label: 'Quit'}
        ]
      },
      {
        label: 'Edit',
        icon: 'fa-edit',
        items: [
          {label: 'Undo', icon: 'fa-mail-forward'},
          {label: 'Redo', icon: 'fa-mail-reply'}
        ]
      }
    ];
  }

  @HostListener('window:keydown', ['$event'])
  hotkeys(event: KeyboardEvent) {
    const letter = String.fromCharCode(event.keyCode);
    if (event.altKey && letter === 'C') {
      this.router.navigateByUrl('/customer');
    } else if (event.altKey && letter === 'D') {
      this.router.navigateByUrl('/dashboard');
    }
  }

}
