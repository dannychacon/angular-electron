import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit {
  customers: Customer[];

  constructor() {
  }

  ngOnInit() {
    this.customers = [{'name': 'Danny Chacon', 'address': 'Barrio Dent', 'creditLimit': 2000, 'discount': 1},
      {'name': 'Joel Chacon', 'address': 'Zona de los Santos', 'creditLimit': 3550, 'discount': 15},
    ];
  }

}


export interface Customer {
  name;
  address;
  creditLimit;
  discount: number;
}

